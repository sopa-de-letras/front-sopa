import { Component, OnInit } from '@angular/core';
import { Form, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SopaService } from '../services/sopa.service';
import { Coor, Sopa } from '../interface/interfaces.interface'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private sopaService : SopaService,
) { }

  ciertas = ['TIBURÓN', 'LEOPARDO', 'PUMA', 'COCODRILO', 'LEÓN',
    'DELFÍN', 'TIGRE', 'ÁGUILA', 'LOBO', 'GUEPARDO'
  ]

  sopa = [
    ['I', 'M', 'X', 'F', 'M', 'A', 'J', 'N', 'L', 'Y', 'G', 'B', 'I','O', 'O'],
    ['J', 'V', 'Ó', 'R', 'U', 'B', 'I', '0', 'F', 'N', 'N', 'Y', 'A','T', 'F'],
    ['C', 'L', 'V', 'Ó', 'U', 'M', 'T', 'L', 'Y', 'L', 'B', 'H', 'D','W', 'J'],
    ['H', 'U', 'V', 'N', 'H', 'O', 'C', 'A', 'Y', 'O', 'B', 'D', 'Y','I', 'G'],
    ['C', 'O', 'V', 'T', 'R', 'D', 'B', 'Z', 'T', 'U', 'U', 'P', 'C','E', 'U'],
    ['E', 'X', 'A', 'C', 'A', 'G', 'H', 'R', 'G', 'F', 'P', 'E', 'O','H', 'E'],
    ['Q', 'X', 'Y', 'V', 'P', 'L', 'I', 'N', 'E', 'Y', 'X', 'V', 'C','W', 'P'],
    ['O', 'B', 'O', 'M', 'O', 'H', 'K', 'Ó', 'E', 'H', 'M', 'H', 'O','M', 'A'],
    ['J', 'X', 'L', 'P', 'E', 'T', 'D', 'E', 'Z', 'T', 'T', 'F', 'D','G', 'R'],
    ['D', 'E', 'T', 'N', 'L', 'W', 'W', 'L', 'S', 'A', 'I', 'G', 'R','W', 'D'],
    ['I', 'M', 'X', 'F', 'M', 'A', 'J', 'N', 'L', 'Y', 'G', 'B', 'I','O', 'O'],
    ['C', 'C', 'P', 'O', 'I', 'U', 'Y', 'I', 'Í', 'B', 'R', 'J', 'L','G', 'K'],
    ['O', 'R', 'Z', 'A', 'W', 'Z', 'U', 'T', 'I', 'F', 'E', 'L', 'O','T', 'G'],
    ['Q', 'A', 'M', 'U', 'P', 'G', 'D', 'O', 'R', 'K', 'L', 'C', 'I','V', 'N'],
    ['S', 'N', 'K', 'N', 'Á', 'Q', 'P', 'G', 'C', 'X', 'H', 'E', 'J','D', 'F'],
    ['Z', 'S', 'P', 'F', 'M', 'L', 'P', 'S', 'S', 'Z', 'T', 'K', 'D','L', 'G'],
   ]

  ren_max = this.sopa.length;
  col_max = this.sopa[0].length;

  formulario !: FormGroup;
  SopaResuelta !: Sopa[];
  pintaCoordenadas : boolean[]=[]; 

  ngOnInit(): void {
    this.creaFormulaio(); 
    //this.borraCoordenadas();
    console.log(this.formulario);
    //this.busca();
    this.construirSopa();
    console.log('ren: '+ this.ren_max);
    console.log('col: '+ this.col_max);
  }

  busca(  ){
  
    for ( let palabra = 0; palabra < this.ciertas.length; palabra++ ){ //palabra = tiburon
      for ( let ren = 0; ren < this.ren_max; ren++ ){
        for ( let col = 0; col < this.col_max; col++ ){
          if ( this.sopa[ren][col] === this.ciertas[palabra][0] ){//encontre la primer letra
            //console.log('buscando... ' + ciertas[palabra])
            //console.log('Encontre la primer letra: '+ ciertas[palabra][0] + '. ren: '+ren+' col: '+col);
            let coor : any[] =[];
            coor[0] = this.encuentraLetraColindante(ren,col,this.ciertas[palabra][0]);
            let i = coor[0].length;
            for ( let letra = 1; letra < this.ciertas[palabra].length; letra++ ){
              for ( let j = 0; j < i; j++ ){
                  coor[letra]  =  this.encuentraLetraColindante( coor[letra-1][j].ren, coor[letra-1][j].col, this.ciertas[palabra][letra] ) 
                  if( i > 1 ){
                      if ( coor[letra].length == 0 ){
                          delete coor[letra-1][j];
                      }
                  }
              }
              if ( coor[letra].length ){
                  i = coor[letra].length;
              }else{
                  i=1;
                  break;
              }
              if ( letra == (this.ciertas[palabra].length-1) ){
                  console.log('encontre : '+ this.ciertas[palabra]);
                  console.log(coor)
              }
            }
          }
        }
      }
    }
  }

  encuentraLetraColindante( ren : number, col : number, letra : string ){
    //console.log( `vamos a buscar a ${letra}, en ren: ${ren}, col: ${col}` )
    let encontro: any[] = [];
    let col_b = col;
    col_b--;
    //3 nivles de comparacion, arriba,abajo, en su mismo eje
    for (let j = 0; j < 3; j++){//col
        let ren_b = ren;
        ren_b--;
        for ( let i = 0; i < 3; i ++ ){//ren
            //compara si esta dentro de un rango que se pueda buscar
            if ( (ren_b >= 0) && ( col_b >= 0 ) ){
                if ( (ren_b < this.ren_max) && (col_b < this.col_max) ){
                    //console.log(`buscando en ren: ${ren_b}, col: ${col_b}`);
                    if ( this.sopa[ren_b][col_b] === letra ){
                        encontro.push({"letra":letra,"ren":ren_b,"col":col_b})
                    }
                }
            }
            ren_b++;
        }
        col_b++;
    }
    return encontro
  }

  get sopaForm(){
    return this.formulario.get('sopaForm') as FormArray;
  }

  get palabraForm(){
    return this.formulario.get('palabraSopaForm') as FormArray;
  }

  creaFormulaio(){

    this.formulario = this.formBuilder.group({

      palabraSopaForm : this.formBuilder.array([
        ['TIBURÓN'],
        ['LEOPARDO'],
        ['PUMA'],
        ['COCODRILO'],
        ['LEÓN'],
        ['DELFÍN'],
        ['TIGRE'],
        ['ÁGUILA'],
        ['LOBO'],
        ['GUEPARDO']
      ]),

      sopaForm :this.formBuilder.array([
     
      ]),
    });

  }

  construirSopa(){
    for ( let ren = 0; ren < this.ren_max; ren++ ){
      for( let col = 0; col < this.col_max; col++ ){
        this.sopaForm.push( this.formBuilder.control(this.sopa[ren][col]) );
      }
    }
  }

  guardar(){
    console.log('hola');
    console.log(this.formulario);
    this.borraCoordenadas();
    this.sopaService.getSopa(this.formulario.value,this.ren_max, this.col_max).subscribe((data:any)=>{
      this.pintaSopa(data);
    });
    return
  }

  borraCoordenadas(){
    this.pintaCoordenadas=[];
    for( let ren = 0; ren < this.ren_max; ren++ ){
      for( let col = 0; col < this.col_max; col++ ){
        this.pintaCoordenadas.push(false);
      }
    }
  }

  pintaSopa( sopa : Sopa[] ){
    for( let letras of sopa ){
      console.log(letras);
      for (let coor of letras.coor){
        for (let i=0; i < coor.length; i++){
          if (coor[i]){
            if ( coor[i]?.letra != false){
              this.pintaCoordenadas[coor[i]!.col + coor[i]!.ren * this.col_max] = true;
            }
          }
        }
      }
    }
  }

  agregarPalabra(){
    this.palabraForm.push( this.formBuilder.control('') )
  }

  eliminarPalabra(i : number){
    this.palabraForm.removeAt(i);
  }

  agregarRenglon(){
    this.ren_max++;
    this.borraCoordenadas();
    for (let col = 0; col < this.col_max; col++){
      this.sopaForm.push( this.formBuilder.control( String.fromCharCode( Math.floor( Math.random() * 25 ) + 65 ) ) )
    }
  }

  eliminarRenglon(){
    this.borraCoordenadas();
    this.ren_max--;
    for (let col = 0; col < this.col_max; col++){
      this.sopaForm.removeAt(-1);
    }
  }

  agregarColumna(){
    // this.borraCoordenadas();
    this.borraCoordenadas();

    this.col_max++;
    let ren = 0;
    for ( ren = 0; ren < this.ren_max; ren++ ){
      try {
      this.sopa[ren].push(String.fromCharCode( Math.floor( Math.random() * 25 ) + 65 ));
      } catch (error) {
      }
      
    }  
    console.log(this.ren_max);
    console.log(ren);
    this.creaFormulaio();
    this.construirSopa();

  }

  eliminarColumna(){
    this.borraCoordenadas();
    this.col_max--;
    console.log(this.col_max);
    this.creaFormulaio();
    this.construirSopa();
  }

  checaColumna( col : number, col_max : number ){
    if ( col > 0 ){
      col ++;
      if ( (  parseInt( (col/col_max).toString() )  ) === ( col/col_max ) ){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }


}
