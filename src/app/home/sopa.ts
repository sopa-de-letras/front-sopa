const ciertas = ['TIBURÓN', 'LEOPARDO', 'PUMA', 'COCODRILO', 'LEÓN',
'DELFÍN', 'TIGRE', 'ÁGUILA', 'LOBO', 'GUEPARDO']

const sopa = [
 ['I', 'M', 'X', 'F', 'M', 'A', 'J', 'N', 'L', 'Y', 'G', 'B', 'I','O', 'O'],
 ['J', 'V', 'Ó', 'R', 'U', 'B', 'I', '0', 'F', 'N', 'N', 'Y', 'A','T', 'F'],
 ['C', 'L', 'V', 'Ó', 'U', 'M', 'T', 'L', 'Y', 'L', 'B', 'H', 'D','W', 'J'],
 ['H', 'U', 'V', 'N', 'H', 'O', 'C', 'A', 'Y', 'O', 'B', 'D', 'Y','I', 'G'],
 ['C', 'O', 'V', 'T', 'R', 'D', 'B', 'Z', 'T', 'U', 'U', 'P', 'C','E', 'U'],
 ['E', 'X', 'A', 'C', 'A', 'G', 'H', 'R', 'G', 'F', 'P', 'E', 'O','H', 'E'],
 ['Q', 'X', 'Y', 'V', 'P', 'L', 'I', 'N', 'E', 'Y', 'X', 'V', 'C','W', 'P'],
 ['O', 'B', 'O', 'M', 'O', 'H', 'K', 'Ó', 'E', 'H', 'M', 'H', 'O','M', 'A'],
 ['J', 'X', 'L', 'P', 'E', 'T', 'D', 'E', 'Z', 'T', 'T', 'F', 'D','G', 'R'],
 ['D', 'E', 'T', 'N', 'L', 'W', 'W', 'L', 'S', 'A', 'I', 'G', 'R','W', 'D'],
 ['I', 'M', 'X', 'F', 'M', 'A', 'J', 'N', 'L', 'Y', 'G', 'B', 'I','O', 'O'],
 ['C', 'C', 'P', 'O', 'I', 'U', 'Y', 'I', 'Í', 'B', 'R', 'J', 'L','G', 'K'],
 ['O', 'R', 'Z', 'A', 'W', 'Z', 'U', 'T', 'I', 'F', 'E', 'L', 'O','T', 'G'],
 ['Q', 'A', 'M', 'U', 'P', 'G', 'D', 'O', 'R', 'K', 'L', 'C', 'I','V', 'N'],
 ['S', 'N', 'K', 'N', 'Á', 'Q', 'P', 'G', 'C', 'X', 'H', 'E', 'J','D', 'F'],
 ['Z', 'S', 'P', 'F', 'M', 'L', 'P', 'S', 'S', 'Z', 'T', 'K', 'D','L', 'G'],
]

const ren_max = sopa.length;
const col_max = sopa[0].length;
console.log( "tamaño sopa en ren:" + ren_max )
console.log( "tamaño sopa en col:" + col_max )

if (1){



for ( let palabra = 0; palabra < 2 /*ciertas.length*/; palabra++ ){ //palabra = tiburon
    for ( let ren = 0; ren < ren_max; ren++ ){
        for ( let col = 0; col < col_max; col++ ){
            if ( sopa[ren][col] === ciertas[palabra][0] ){//encontre la primer letra
                console.log('buscando... ' + ciertas[palabra])
                console.log('Encontre la primer letra: '+ ciertas[palabra][0] + '. ren: '+ren+' col: '+col);
                let coor : any[] =[];
                coor[0] = encuentraLetraColindante(ren,col,ciertas[palabra][0]);
                let i = coor[0].length;
                for ( let letra = 1; letra < ciertas[palabra].length; letra++ ){
                    for ( let j = 0; j < i; j++ ){
                        coor[letra]  =  encuentraLetraColindante( coor[letra-1][j].ren, coor[letra-1][j].col, ciertas[palabra][letra] ) 
                        console.log(coor)
                        if( i > 1 ){
                            if ( coor[letra].length == 0 ){
                                delete coor[letra-1][j];
                            }
                        }
                    }
                    if ( coor[letra].length ){
                        i = coor[letra].length;
                    }else{
                        i=1;
                        break;
                    }
                }
            }
        }
    }
}

}

/*const coordenada = encuentraLetraColindante(1,4,'U')
console.log(coordenada)
console.log(coordenada[1].ren)*/

function encuentraLetraColindante( ren : number, col : number, letra : string ){
    console.log( `vamos a buscar a ${letra}, en ren: ${ren}, col: ${col}` )
    let encontro: any[] = [];
    let col_b = col;
    col_b--;
    //3 nivles de comparacion, arriba,abajo, en su mismo eje
    for (let j = 0; j < 3; j++){//col
        let ren_b = ren;
        ren_b--;
        for ( let i = 0; i < 3; i ++ ){//ren
            //compara si esta dentro de un rango que se pueda buscar
            if ( (ren_b >= 0) && ( col_b >= 0 ) ){
                if ( (ren_b < ren_max) && (col_b < col_max) ){
                    //console.log(`buscando en ren: ${ren_b}, col: ${col_b}`);
                    if ( sopa[ren_b][col_b] === letra ){
                        encontro.push({"letra":letra,"ren":ren_b,"col":col_b})
                    }
                }
            }
            ren_b++;
        }
        col_b++;
    }
    return encontro
}
