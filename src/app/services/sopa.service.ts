import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class SopaService {

  private url = "http://localhost:3000";

  constructor( private http : HttpClient ) { 
    console.log('Servicio Sopa Redis');
  }

  getSopa( sopa : any, ren:number, col:number ){
    return this.http.post(`${this.url}/sopa/${ren}/${col}`,sopa)
  }

}
