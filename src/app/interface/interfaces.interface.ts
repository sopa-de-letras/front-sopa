export interface Sopa {
    palabra: string;
    coor:    Array<Array<Coor | null>>;
}

export interface Coor {
    letra: string | boolean;
    ren:   number;
    col:   number;
}